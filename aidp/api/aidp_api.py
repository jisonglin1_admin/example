import yaml

from aidp.api.base_api import BaseApi
from aidp.utils.logger import logger


class AidpApi(BaseApi):
    username="uRX+OEtFkEvlgBCJzrCLKg==",
    password="qgjKYYAPPv6kMxL+hdNmEQ=="
    base_url="https://172.16.3.97:1999"
    def __init__(self):
        logger.info("初始化-----获取aidp系统的token")
        self.get_token()

    def aidp_requests(self,method,url,**kwargs):
        if "headers" in kwargs:
            kwargs["headers"]["Cookie"]=f"JSESSIONID={self.token}"
        else:
            kwargs["headers"]={"Cookie":f"JSESSIONID={self.token}"}
        r=self.base_requests(method=method,url=url,**kwargs,verify=False)
        logger.info(r.json())
        return r.json()


    def get_token(self):
        token_url=self.base_url+"/api/base/token"
        json_data={
            "username":self.username,
            "password":self.password
        }
        r=self.base_requests(method="post",url=token_url,json=json_data,verify=False)
        self.token = r.json().get("data").get("id")
        logger.info(f"获取token的响应信息为{self.token}")
        return self.token

