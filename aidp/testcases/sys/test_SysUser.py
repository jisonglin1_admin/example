# -*- coding:utf-8 -*-
from aidp.api.sys.SysUser_api import SysUserApi
import json
import pytest
from faker import Faker
from aidp.utils.getdata import get_data
from aidp.utils.logger import logger
import  allure

@allure.feature("用户管理")
class TestSysUser:
    address="../../data/sys/SysUser.yaml"

    # 前置，作用域为class
    def setup_class(self):
        self.sysuser=SysUserApi()
        self.fake=Faker("zh_CN")


    @allure.story("获取用户列表")
    @pytest.mark.own("吉松林")
    @pytest.mark.parametrize("kwargs",get_data(address)["get_list"]["success"]["param"])
    def test_get_list(self,kwargs):
        r=self.sysuser.get_list(**kwargs)
        assert r.get("code")==0

    @allure.story("新增用户")
    @pytest.mark.own("吉松林")
    def test_create(self,get_unique_member):
        # 随机手机号
        mobile = self.fake.phone_number()
        # 随机邮箱号
        email=self.fake.email()
        # 随机地址
        address=self.fake.address()
        with allure.step(f"新增用户{get_unique_member}"):
            r=self.sysuser.create(username=get_unique_member,mobile=mobile,email=email,address=address)
        assert r.get("code")==0

    @allure.story("删除用户")
    @pytest.mark.own("吉松林")
    def test_delete(self):
        r=self.sysuser.delete()
        assert r.get("code") == 0


    @allure.story("查看用户信息")
    @pytest.mark.own("吉松林")
    def test_get(self,get_unique_member):
        r=self.sysuser.get(params=get_unique_member)
        assert r.get("code")==0

    @allure.story("更新用户")
    @pytest.mark.own("吉松林")
    @pytest.mark.parametrize("kwargs", get_data(address)["update"]["success"]["param"])
    def test_update(self,kwargs,get_unique_member):
        r=self.sysuser.update(get_unique_member,**kwargs)
        assert  r.get("code")==0


    @allure.story("用户禁用")
    @pytest.mark.own("吉松林")
    def test_disbale(self,get_unique_member):
        r=self.sysuser.disable(params=get_unique_member)
        assert r.get("code")==0