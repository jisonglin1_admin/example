import json

from feishu.api.base_api import BaseApi
from feishu.utils.logger import logger


class FeishuApi(BaseApi):
    app_id="cli_a1f552c70078500e"
    app_secret="wIKyWoUVNAorC9QA42O8fgY4CYwYgwCg"
    def feishu_requests(self,method,url,**kwargs):
        if "headers" in kwargs:
            kwargs["headers"]["Authorization"] = f"Bearer {self.get_token()}"
        else:
            kwargs["headers"] = {"Authorization": f"Bearer {self.get_token()}"}

        r=self.base_requests(method=method,url=url,**kwargs)
        return r.json()

    def get_token(self):
        token_url="https://open.feishu.cn/open-apis/auth/v3/tenant_access_token/internal"
        json_data={
            "app_id":self.app_id,
            "app_secret":self.app_secret
        }
        r=self.base_requests(method="post",url=token_url,json=json_data)
        self.token = r.json()["tenant_access_token"]
        logger.info(f"获取token的响应信息为{r.json()}")
        return self.token