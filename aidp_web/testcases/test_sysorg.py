# -*- coding:utf-8 -*-
import pytest
from faker import Faker
import allure
from aidp_web.pageobject.mainpage import MainPage

@allure.feature("机构管理")
class TestOrg:
    def setup_class(self):
        self.main=MainPage().goto_sysorg_page()
        self.fake = Faker('zh_CN')

    def teardown_class(self):
        self.main.close_driver()

    @allure.story("新增机构")
    @allure.title("新增机构")
    @pytest.mark.own("jisonglin")
    @pytest.mark.run(order=1)
    @pytest.mark.parametrize("name,num,remark",[("测试机构","3","吉松林创建")])
    def test_create(self,name,num,remark):
        r=self.main.add_org(name,num,remark).get_org()
        assert "测试机构" in r

    @allure.story("删除机构")
    @allure.title("删除机构")
    @pytest.mark.own("jisonglin")
    @pytest.mark.run(order=3)
    @pytest.mark.parametrize("name",[("测试机构1")])
    def test_delete(self,name):
        self.main.delete_org(name)

    @allure.story("更新机构")
    @allure.title("更新机构")
    @pytest.mark.own("jisonglin")
    @pytest.mark.run(order=2)
    @pytest.mark.parametrize("original_name,name,num,remark", [("测试机构","测试机构1","2","吉松林修改")])
    def test_update(self,original_name,name,num,remark):
        self.main.update_org(original_name,name,num,remark)

    @allure.story("新增删除用户")
    @allure.title("新增删除用户")
    @pytest.mark.own("jisonglin")
    def test_adduser_and_delete(self,get_unique_member):
        realname = self.fake.name()
        iphone=self.fake.phone_number()
        email=self.fake.email()
        password="test1234"
        name="test"+str(get_unique_member)
        self.main.add_user(name,password,realname,email,iphone)
        self.main.delete_user(realname)


