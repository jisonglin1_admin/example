# -*- coding: utf-8 -*-
import threading
import time

import pytest

from aidp.api.aidp_api import AidpApi
from aidp.utils.logger import logger


def pytest_collection_modifyitems(items):
    '''
    :param items:
    :return:
    '''
    for item in items:
        item.name = item.name.encode("utf-8").decode("unicode_escape")
        item._nodeid = item.nodeid.encode("utf-8").decode("unicode_escape")



@pytest.fixture()
def get_unique_member():
    userid=str(round(time.time()))+threading.current_thread().name
    return userid