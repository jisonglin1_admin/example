# -*- coding:utf-8 -*-
import yaml
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.remote.webelement import WebElement
from aidp_web.utils.logger import logger
from selenium import webdriver


class BasePage:
    base_url="https://172.16.3.97:1999"
    driver = webdriver.Chrome()
    driver.get(f"{base_url}/#/login")
    driver.implicitly_wait(10)
    driver.maximize_window()
    driver.find_element_by_id("details-button").click()
    driver.find_element_by_id("proceed-link").click()
    driver.find_element_by_xpath("//input[@placeholder='请输入用户名']").send_keys("policeman")
    driver.find_element_by_xpath("//input[@placeholder='请输入密码']").send_keys('sdyjs4567')
    driver.find_element_by_xpath("//input[@placeholder='验证码']").send_keys("1")
    driver.find_element_by_xpath("//div[@class='anim']").click()


    def find(self,by,locator=None):
        if locator is None:
            logger.debug(f"元素定位为{by}")
            return self.driver.find_element(*by)
        else:
            logger.debug(f"元素定位为{by},{locator}")
            return self.driver.find_element(by.locator)

    def wait_for_time(self, locator, timeout=10):
        element: WebElement = WebDriverWait(self.driver, timeout).until(
            expected_conditions.element_to_be_clickable(locator))
        return element


    def finds(self,by,locator=None):
        if locator is None:
            eles=self.driver.find_elements(*by)
            return eles
        else:
            eles=self.driver.find_elements(by.locator)
            return eles


    def close_driver(self):
        self.driver.close()
