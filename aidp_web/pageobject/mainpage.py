# /usr/bin/env python
# -*- coding:utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By

from aidp_web.pageobject.basepage import BasePage
from aidp_web.pageobject.sysorg_page import SysOrgPage
from time import sleep

from aidp_web.pageobject.sysuser_page import SysUserPage


class MainPage(BasePage):

    # 系统机构管理路径
    _url1 = (f"{BasePage().base_url}/#/sysManage/orgManage?menuId=36ab80d476bbf7087b8f198317db5585&configId=")
    # 角色分配管理
    _url2 = (f"{BasePage().base_url}/#/sysManage/roleManage?menuId=561bf944a7c94c2c906d0bb068be4017&configId=")
    # 用户管理
    _url3 = (f"{BasePage().base_url}/#/sysManage/userManage?menuId=6394a825ffe02d3149286c42cda870b1&configId=")

    def goto_sysorg_page(self):
        sleep(1)
        self.driver.get(self._url1)
        sleep(3)
        return SysOrgPage()

    # def goto_sysrole_page(self):
    #     self.driver.get(self._url2)
    #     return SysRolePage(self.driver)
    #
    def goto_sysuser_page(self):
        self.driver.get(self._url3)
        return SysUserPage()


