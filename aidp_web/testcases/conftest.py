import time

import pytest


def pytest_collection_modifyitems(items):
    '''
    :param items:
    :return:
    '''
    for item in items:
        item.name = item.name.encode("utf-8").decode("unicode_escape")
        item._nodeid = item.nodeid.encode("utf-8").decode("unicode_escape")

@pytest.fixture()
def get_unique_member():
    number=str(round(time.time()))
    return number