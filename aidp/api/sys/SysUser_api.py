# -*- coding:utf-8 -*-
from aidp.api.aidp_api import AidpApi
from aidp.utils.connectdb import connect_db


class SysUserApi(AidpApi):
    def create(self,username="jisonglin",
               password="qgjKYYAPPv6kMxL+hdNmEQ==",
               email="",
               mobile="",
               realName="吉松林",
               address="",
               orgId="b35313b2dcac6d11041fa75e63e4c415",
               status=1,
               sex=1,
               roleIdList=[],
               ):
        url=self.base_url+"/api/v1/sys/user/save"
        method="post"
        json_data={
            "username":username,
            "password":password,
            "email":email,
            "mobile":mobile,
            "realName":realName,
            "address":address,
            "orgId":orgId,
            "status":status,
            "sex":sex,
            "roleIdList":roleIdList
        }
        r = self.aidp_requests(method=method, url=url, json=json_data)
        return r

    def delete(self):
        sql="select user_id from sys_user where username='jisonglin'"
        url=self.base_url+"/api/v1/sys/user/remove"
        method="post"
        self.create()
        # 获取数据库字段user_id
        data=connect_db(sql)
        json_data=data
        r = self.aidp_requests(method=method, url=url, json=json_data)
        return r


    def update(self,params,realName="ceshi",email="ceshi@qq.com"):
        url=self.base_url+"/api/v1/sys/user/update"
        method="post"
        r=self.get(params)
        data=r.get("rows")
        data["RealName"]=realName
        data["email"]=email
        json_data=data
        print(data)
        r = self.aidp_requests(method=method, url=url, json=json_data)
        return r


    def get(self,params):
        sql = f"select user_id from sys_user where username='{params}'"
        method="get"
        self.create(username=params)
        data = connect_db(sql)
        data=data[0]
        url=self.base_url+f"/api/v1/sys/user/infoUser?userId={data}"
        r = self.aidp_requests(method=method, url=url)
        return r


    def get_list(self,roleId="",
                   mobile="",
                   orgId="",
                   realName="",
                   userName="",
                   pageNumber=1,
                   pageSize=10,
                   sortOrder="asc"):
        url=self.base_url+"/api/v1/sys/user/list"
        method="post"
        json_data={"roleId":roleId,
                   "mobile":mobile,
                   "orgId":orgId,
                   "realName":realName,
                   "userName":userName,
                   "pageNumber":pageNumber,
                   "pageSize":pageSize,
                   "sortOrder":sortOrder}
        r=self.aidp_requests(method=method,url=url,json=json_data)
        return r

    def disable(self,params):
        self.create(username=params,realName="禁用用户")
        sql = f"select user_id from sys_user where username='{params}'"
        url = self.base_url+"/api/v1/sys/user/disable"
        method="post"
        data = connect_db(sql)
        json_data = data
        r = self.aidp_requests(method=method, url=url, json=json_data)
        return r

    def enable(self,params):
        self.disable(params)
        url=self.base_url+"/api/v1/sys/user/enable"
        method="post"




