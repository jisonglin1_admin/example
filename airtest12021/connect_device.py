#!/usr/bin/env python
# encoding: utf-8

import pytest

from airtest.core.api import *
from airtest.core.api import auto_setup
from poco.drivers.android.uiautomation import AndroidUiautomationPoco
poco = AndroidUiautomationPoco(use_airtest_input=True, screenshot_each_action=False)

class TestAirtest:
    def setup(self):
        auto_setup(__file__,logdir=True,devices=[
            "Android:///127.0.0.1:7555"
        ])



    def test_1(self):
        start_app("com.xueqiu.android")

    def test_2(self):
        print("点击行情")
        poco(text='行情').click()

    def test_3(self):
        print("点击交易")
        poco(text="交易").wait(3).click()

    def test_4(self):
        print("返回到主页")
        home()