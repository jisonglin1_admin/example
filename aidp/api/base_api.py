#! /usr/bin/env python
# -*- coding:utf-8 -*-
import requests

from aidp.utils.logger import logger


class BaseApi:
    def base_requests(self,method,url,**kwargs):
        logger.debug(f"请求的方法为{method},请求的url为{url},请求参数为{kwargs}")
        r=requests.request(method,url,**kwargs)
        return r