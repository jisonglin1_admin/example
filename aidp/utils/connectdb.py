# -*- coding:utf-8 -*-

# 获取db.yaml文件内容
import pymysql
import os
from aidp.utils.getdata import get_data


def connect_db(sql):
    curPath = os.path.abspath(os.path.dirname(__file__))
    address=f"{curPath}/../data/db/db.yaml"
    data=get_data(address)
    print(data.get("db"))
    if data.get("db")=="mysql":
        conn = pymysql.connect(
            host=data.get("host"),
            database=data.get("database"),
            user=data.get("user"),
            passwd=data.get("passwd"),
            port=data.get("port"),
            charset='utf8')
        # 游标
        cur = conn.cursor()
        cur.execute(sql)
        result = cur.fetchall()
        result=(list(*result))
        cur.close()
        conn.close()
        return result



