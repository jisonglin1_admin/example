#! /usr/bin/env python
# -*- coding:utf-8 -*-
import allure
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from time import  sleep
from aidp_web.pageobject.basepage import BasePage
from aidp_web.utils.logger import logger


class SysOrgPage(BasePage):
    # 新增机构按钮
    _ADDBUTTON=(By.XPATH,"//div[@class='el-button-group']//button[@type='button']/span[text()=' 新增 ']")
    # 编辑机构按钮
    _UPDATEBUTTON=(By.CSS_SELECTOR,'.el-button-group .icon-edit')
    # 新增机构名称
    _INPUTNAME=(By.XPATH,"//div[@class='el-form-item is-required'][1]/div/div/input")
    # 新增排序
    _INPUTORDER=(By.XPATH,'/html/body/div[2]/div/div/div/div[2]/div[2]/div/div[1]/form/div[4]/div/div/input')
    # 点击机构列表
    _CLICKORG=(By.XPATH,"//span[text()='机构列表' and @class='el-tooltip MyTooltip-txt one-row']")

    # 新增备注
    _REMARK=(By.XPATH,"//textarea[@placeholder='此处用于填写相关的备注信息......']")

    # 新增确定按钮
    _DEFINEBUTTON=(By.XPATH,"//span[text()='确定']")

    # 机构列表
    _ORG_LIST=(By.XPATH,"//div[@class='el-tree-node__children']//div[@role='treeitem']")

    # 删除机构按钮
    _DELETE_ORG=(By.XPATH,"//div[@class='el-button-group']//button[@type='button']/span[text()=' 删除 ']")

# 编辑窗口
    # 编辑窗口名称字段
    _EDIT_NAME=(By.XPATH,"//div[@class='ant-modal-body']//form/div[1]/div/div/input")
    # 编辑窗口排序号字段
    _EDIT_ORDER = (By.XPATH, "//div[@class='ant-modal-body']//form/div[4]/div/div/input")
    # 编辑窗口备注字段
    _EDIT_REMARK=(By.CSS_SELECTOR,".el-form-item .el-textarea__inner")
    # 确认按钮
    _SAVEBUTTON = (By.CSS_SELECTOR, '.ant-modal-footer .confirm')

# 新增用户窗口
    # 新增用户按钮
    _ADDUSERBUTTON=(By.XPATH,"//span[@class='df aic']//button[@class='el-button el-button--default']")
    # 新增用户
    _ADDUSER=(By.XPATH,"//form[@class='el-form demo-ruleForm el-form--label-right el-form--inline']/div[1]//input")
    # 新增用户密码
    _ADDUSERPASSWD=(By.XPATH,"//form[@class='el-form demo-ruleForm el-form--label-right el-form--inline']/div[2]//input")
    # 新增用户邮箱
    _ADDUSEREMAIL=(By.XPATH,"//form[@class='el-form demo-ruleForm el-form--label-right el-form--inline']/div[3]//input")
    # 新增用户手机号
    _ADDUSERIPHONE = (By.XPATH, "//form[@class='el-form demo-ruleForm el-form--label-right el-form--inline']/div[4]//input")
    # 新增用户真实姓名
    _ADDREALUSER=(By.XPATH,"//form[@class='el-form demo-ruleForm el-form--label-right el-form--inline']/div[5]//input")
    # 状态正常
    _STATUS=(By.XPATH,"//span[text()='正常']")
    # 确定按钮
    _CONFIRMBUTTON=(By.XPATH,"//span[text()='确定']")


    def add_org(self,name,num,remark):
        with allure.step("点击机构列表"):
            self.find(self._CLICKORG).click()
        with allure.step("点击新增机构"):
            self.wait_for_time(self._ADDBUTTON).click()
        with allure.step(f"新增窗口添加名称{name}"):
            self.wait_for_time(self._INPUTNAME).send_keys(name)
        with allure.step(f"新增窗口添加排序号{num}"):
            self.wait_for_time(self._INPUTORDER).send_keys(num)
        with allure.step(f"新增窗口添加备注{remark}"):
            self.wait_for_time(self._REMARK).send_keys(remark)
        with allure.step(f"新增窗口点击确定"):
            self.find(self._DEFINEBUTTON).click()
        return SysOrgPage()

    def get_org(self):
        # 刷新页面
        self.driver.refresh()
        org_list=[]
        eles=self.finds(self._ORG_LIST)
        for ele in eles:
            org_list.append(ele.get_attribute('innerText'))
        logger.info(f"输出{org_list}")
        return org_list

    def delete_org(self,name):
        # self.driver.refresh()
        # 机构
        _TEMP_ORG=(By.XPATH,f"//span[text()='{name}']")
        with allure.step(f"点击需要删除的机构"):
            self.wait_for_time(_TEMP_ORG).click()
        with allure.step(f"点击删除机构按钮"):
            self.wait_for_time(self._DELETE_ORG).click()
        with allure.step(f"点击删除弹框中的确定按钮"):
            self.wait_for_time(self._CONFIRMBUTTON).click()

    def update_org(self,original_name,name,num,remark):
        # 机构
        _TEMP_ORG=(By.XPATH,f"//span[text()='{original_name}']")
        with allure.step(f"点击需要编辑的机构{original_name}"):
            self.find(_TEMP_ORG).click()
        with allure.step(f"点击编辑机构按钮"):
            self.find(self._UPDATEBUTTON).click()
        with allure.step(f"编辑窗口输入机构名{name}"):
            self.wait_for_time(self._EDIT_NAME).clear()
            self.wait_for_time(self._EDIT_NAME).send_keys(name)
        with allure.step(f"编辑窗口输入排序号{num}"):
            self.wait_for_time(self._EDIT_ORDER).send_keys(num)
        with allure.step(f"编辑窗口输入备注{remark}"):
            self.wait_for_time(self._EDIT_REMARK).send_keys(remark)
        with allure.step("点击保存按钮"):
            self.wait_for_time(self._SAVEBUTTON).click()




    def add_user(self,name,password,realname,email,iphone):
        self.driver.refresh()
        with allure.step(f"点击编辑机构按钮"):
            self.find(self._CLICKORG).click()
        with allure.step(f"点击添加用户按钮"):
            self.find(self._ADDUSERBUTTON).click()
        with allure.step(f"输入用户名{name}"):
            self.find(self._ADDUSER).send_keys(name)
        with allure.step(f"输入用户名密码{password}"):
            self.find(self._ADDUSERPASSWD).send_keys(password)
        with allure.step(f"输入邮箱{email}"):
            self.find(self._ADDUSEREMAIL).send_keys(email)
        with allure.step(f"输入电话号码{iphone}"):
            self.find(self._ADDUSERIPHONE).send_keys(iphone)
        with allure.step(f"真实姓名{realname}"):
            self.find(self._ADDREALUSER).send_keys(realname)
        with allure.step(f"选择状态"):
            self.find(self._STATUS).click()
        with allure.step(f"点击确认按钮"):
            self.find(self._CONFIRMBUTTON).click()


    def delete_user(self,delete_name):
        self.driver.refresh()
        # 删除用户的删除按钮位置
        with allure.step("点击机构管理"):
            self.find(self._CLICKORG).click()
        delete_user_button=(By.XPATH,f"//*[text()='{delete_name}']/../../../../td[8]/div/a[5]")
        with allure.step(f"删除{delete_name}"):
            self.wait_for_time(delete_user_button).click()
        # sleep(2)
        try:
            self.wait_for_time(self._CONFIRMBUTTON).click()
        except Exception:
            logger.warning("出错了")
            self.wait_for_time(self._CONFIRMBUTTON).click()




