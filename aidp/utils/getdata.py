# -*- coding:utf-8 -*-
import yaml
def get_data(address):
    with open(f"{address}", mode='r', encoding="gbk") as f:
        return yaml.safe_load(f)